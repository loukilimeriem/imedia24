package de.imedia24.shop.controller

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.entity.StockEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.db.repository.StockRepository
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.math.BigDecimal
import java.time.ZonedDateTime

@SpringBootTest
@AutoConfigureMockMvc
class ProductControllerTest {
    @Autowired
    private lateinit var mockMvc: MockMvc
    @Autowired
    private lateinit var productRepository: ProductRepository
    @Autowired
    private lateinit var stockRepository: StockRepository

    @BeforeEach
    fun init() {
        stockRepository.save(StockEntity("1","Desc1",10, ZonedDateTime.now(), ZonedDateTime.now()))
        stockRepository.save(StockEntity("2","Desc2",20, ZonedDateTime.now(), ZonedDateTime.now()))
        stockRepository.save(StockEntity("3","Desc3",10, ZonedDateTime.now(), ZonedDateTime.now()))

        productRepository.save(ProductEntity(sku = "sku1" ,name = "P1", description = "First Product", price = BigDecimal(1000), ZonedDateTime.now(), ZonedDateTime.now() ,stockRepository.findById("1").get()))
        productRepository.save(ProductEntity(sku = "sku2" ,name = "P2", description = "Second Product", price = BigDecimal(2000), ZonedDateTime.now(), ZonedDateTime.now(),stockRepository.findById("2").get()  ))
        productRepository.save(ProductEntity(sku = "sku3" ,name = "P3", description = "Third Product", price = BigDecimal(3000) , ZonedDateTime.now(), ZonedDateTime.now() ,null))

    }

    @Test
    fun testfindProductsByListOfSkus(){
        mockMvc.perform(MockMvcRequestBuilders.get("/products")

                .contentType("application/json")
                .param("skus", "sku1")
                .param("skus", "sku2"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.jsonPath("$.size()").value(2))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].sku").value("sku1"));
    }
}