package de.imedia24.shop.db.entity

import org.hibernate.annotations.UpdateTimestamp
import java.math.BigDecimal
import java.time.ZonedDateTime
import javax.persistence.*

@Entity
@Table(name = "products")
data class ProductEntity  (
    @Id
    @Column(name = "sku", nullable = false)
    val sku: String,

    @Column(name = "name", nullable = false)
    var name: String,

    @Column(name = "description")
    var description: String? = null,

    @Column(name = "price", nullable = false)
    var price: BigDecimal,

    @UpdateTimestamp
    @Column(name = "created_at", nullable = false)
    var createdAt: ZonedDateTime,

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = false)
    var updatedAt: ZonedDateTime,

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_stock")
    val stock : StockEntity ? = null
){
   public  constructor () : this("","","", BigDecimal(0),ZonedDateTime.now(),
           ZonedDateTime.now(),null)
}
