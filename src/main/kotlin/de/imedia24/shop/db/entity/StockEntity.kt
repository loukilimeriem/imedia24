package de.imedia24.shop.db.entity

import org.hibernate.annotations.UpdateTimestamp
import java.time.ZonedDateTime
import javax.persistence.*

@Entity
@Table(name = "stocks")
class StockEntity (
    @Id
    @Column(name = "id", nullable = false)
    val id: String,

    @Column(name = "description")
    val description: String? = null,

    @Column(name = "available", nullable = false)
    val available: Int,

    @UpdateTimestamp
    @Column(name = "created_at", nullable = false)
    val createdAt: ZonedDateTime,

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = false)
    val updatedAt: ZonedDateTime,

    @OneToMany(mappedBy = "stock", cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
    var products: MutableList<ProductEntity> = mutableListOf(),
){
    public  constructor () : this("","",0,ZonedDateTime.now(),
            ZonedDateTime.now(),mutableListOf())
}