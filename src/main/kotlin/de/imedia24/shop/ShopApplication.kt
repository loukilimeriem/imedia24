package de.imedia24.shop

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.entity.StockEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.db.repository.StockRepository
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import java.math.BigDecimal
import java.time.ZonedDateTime

@SpringBootApplication
class ShopApplication{
}

fun main(args: Array<String>) {
	runApplication<ShopApplication>(*args)
}

