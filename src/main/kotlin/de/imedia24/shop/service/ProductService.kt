package de.imedia24.shop.service

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import org.springframework.stereotype.Service
import java.time.ZonedDateTime

@Service
class ProductService(private val productRepository: ProductRepository) {

    fun findProductBySku(sku: String): ProductResponse? {
        val productEntity = productRepository.findBySku(sku)
        if (productEntity != null) {
            return productEntity.toProductResponse()
        }else{
            return null
        }
    }

    fun findProductsBySkus(skus: List<String>): List<ProductResponse>? {
        val productEntities = productRepository.findAllBySkuIn(skus)
        val productResponses : MutableList<ProductResponse> = mutableListOf()

        if (productEntities != null) {
            for(productEntity in productEntities){
                productEntity.toProductResponse()?.let { productResponses.add(it) }
            }
        }
        return productResponses
    }

    fun addProduct(productResponse: ProductResponse): ProductResponse? {
        return   productRepository.save(productResponse.toProductEntity()).toProductResponse()
    }

    fun updateProduct(productResponse: ProductResponse): ProductResponse? {
        val productEntityFinal : ProductEntity? = productRepository.findBySku(productResponse.sku)
        if( productEntityFinal != null){
            if( productResponse.name != null){
                productEntityFinal.name = productResponse.name!!
            }
            if(productResponse.description != null){
                productEntityFinal.description = productResponse.description
            }
            if(productResponse.price != null){
                productEntityFinal.price = productResponse.price!!
            }
            productEntityFinal.updatedAt = ZonedDateTime.now()
            productRepository.save(productEntityFinal).toProductResponse()
        }else{
            return null
        }
        return   productEntityFinal.toProductResponse()
    }
}
