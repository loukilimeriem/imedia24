package de.imedia24.shop.domain.product

import de.imedia24.shop.db.entity.StockEntity
import java.time.ZonedDateTime

class StockResponse (
    val id: String,
    val description: String?,
    val available: Int
){
    companion object {
        fun StockEntity.toStockResponse() =  StockResponse(
                    id = id,
                    description = description ,
                    available = available
            )
        }

    fun toStockEntity() : StockEntity{
       return StockEntity(id, description, available, ZonedDateTime.now(),ZonedDateTime.now())
    }
}
