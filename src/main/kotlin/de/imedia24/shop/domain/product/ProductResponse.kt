package de.imedia24.shop.domain.product

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.domain.product.StockResponse.Companion.toStockResponse
import java.math.BigDecimal
import java.time.ZonedDateTime

data class ProductResponse(
        var sku: String ,
        var name: String ?,
        var description: String?,
        var price: BigDecimal?,
        var stock : StockResponse?
) {
    companion object {
        fun ProductEntity.toProductResponse() = stock?.let {
            ProductResponse(
                    sku = sku,
                    name = name,
                    description = description ?: "",
                    price = price,
                    stock =  stock.toStockResponse()
            )
        }
    }
    fun toProductEntity() :ProductEntity{
       return ProductEntity(sku,name!!, description!!,price!!, ZonedDateTime.now(),
                              ZonedDateTime.now(), stock!!.toStockEntity())
    }
}
