package de.imedia24.shop.controller

import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.service.ProductService
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.http.MediaType


@RestController
@RequestMapping ("products")
class ProductController(private val productService: ProductService) {

    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!

    @GetMapping("{sku}", produces = ["application/json;charset=utf-8"])
    fun findProductsBySku(
        @PathVariable("sku") sku: String
    ): ResponseEntity<ProductResponse> {
        logger.info("Request for product $sku")

        val product = productService.findProductBySku(sku)
        return if(product == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)
        }
    }

    @GetMapping( produces = ["application/json;charset=utf-8"])
    fun findProductsByListOfSkus( @RequestParam("skus") skus: List<String>
    ): ResponseEntity<List<ProductResponse>> {
        logger.info("Request for products $skus")

        val products = productService.findProductsBySkus(skus)
        return if(products == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(products)
        }
    }

    @PostMapping( produces = ["application/json;charset=utf-8"],consumes = [MediaType.APPLICATION_JSON_VALUE])
    public fun addProduct( @RequestBody product: ProductResponse): ResponseEntity<ProductResponse> {
        logger.info("save new product $product")

        val products = productService.addProduct(product)
        return if(products == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(products)
        }
    }

    @PutMapping( produces = ["application/json;charset=utf-8"],consumes = [MediaType.APPLICATION_JSON_VALUE])
    public fun updateProduct( @RequestBody product: ProductResponse): ResponseEntity<ProductResponse> {
        logger.info("save new product $product")

        val products = productService.updateProduct(product)
        return if(products == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(products)
        }
    }
}
